# Readme
This project breaks out the SD card and it's interface to allow hooking up external wires. Some NAND controllers provide such an interface and with this board you can directly connect them.

**Please keep in mind that you'll need to set the PCB thickness to 0.8mm when ordering.** 

![Schematic](https://gitlab.com/timkoers/sd_card_pcb/-/raw/master/Schematic.svg/SD_Card_PCB.svg)