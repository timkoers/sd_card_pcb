EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x08_Female J1
U 1 1 5F18B32C
P 7800 2850
F 0 "J1" H 7828 2826 50  0000 L CNN
F 1 "Conn_01x08_Female" H 7828 2735 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7800 2850 50  0001 C CNN
F 3 "~" H 7800 2850 50  0001 C CNN
	1    7800 2850
	1    0    0    -1  
$EndComp
$Comp
L microSD_Card:microSD_Card X1
U 1 1 5F18CB5C
P 6250 2950
F 0 "X1" H 6067 2089 50  0000 C CNN
F 1 "microSD_Card" H 6067 2180 50  0000 C CNN
F 2 "Footprints:microSD_Card" H 6067 2271 50  0000 C CNN
F 3 "http://portal.fciconnect.com/Comergent//fci/drawing/10067847.pdf" H 6067 2370 60  0000 C CNN
	1    6250 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 2550 7600 2550
Wire Wire Line
	7600 2650 7250 2650
Wire Wire Line
	7250 2750 7600 2750
Wire Wire Line
	7600 2850 7250 2850
Wire Wire Line
	7250 2950 7600 2950
Wire Wire Line
	7600 3050 7250 3050
Wire Wire Line
	7250 3150 7600 3150
Wire Wire Line
	7600 3250 7250 3250
Text Label 7250 3250 0    50   ~ 0
DAT2
Text Label 7250 3150 0    50   ~ 0
CD\DAT3
Text Label 7250 3050 0    50   ~ 0
CMD
Text Label 7250 2950 0    50   ~ 0
VDD
Text Label 7250 2850 0    50   ~ 0
CLK
Text Label 7250 2750 0    50   ~ 0
VSS
Text Label 7250 2650 0    50   ~ 0
DAT0
Text Label 7250 2550 0    50   ~ 0
DAT1
$EndSCHEMATC
